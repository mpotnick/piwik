<?php

namespace CrossChx;

require 'vendor/autoload.php';

use Composer\Factory;
use Composer\Script\Event;
use Aws\S3\S3Client;
use Aws\Command;
use Aws\S3\ObjectUploader;
use Eloquent\Composer\Configuration\ConfigurationReader;

class PiwikUtils
{

    public static function package(Event $event)
    {

        $reader = new ConfigurationReader();
        $config = $reader->read("./composer.json");

        echo "Creating piwik.zip for version " . $config->version();

        $zip = new \ZipArchive();
        $path = "./dist/piwik.zip";

        if (!$zip->open($path, \ZipArchive::CREATE | \ZipArchive::OVERWRITE)){
            die("Failed to create archive\n");
        }

        $dir = new \RecursiveDirectoryIterator("./");
        $filter = new ZipRecursiveFilterIterator($dir);
        $files = new \RecursiveIteratorIterator($filter, \RecursiveIteratorIterator::LEAVES_ONLY);
        foreach($files as $file) {
            if (!$file->isDir()) {
                $zip->addFile($file);
            }
        }

        if (!$zip->status == \ZipArchive::ER_OK) {
            echo "Failed to write files to zip\n";
        }

        $zip->close();
    }

    public static function deploy(){

        $reader = new ConfigurationReader();
        $config = $reader->read("./composer.json");

        if( !file_exists('./dist/piwik.zip') ){
            die("There's nothing to deploy! Please run `composer package` first.\n");
        }

        echo "Deploying piwik.zip to `crosschx-artifacts/piwik/" . $config->version() . "/piwik.zip`\n";

        $s3 = new S3Client([
            'profile' => 'default',
            'version' => 'latest',
            'region'  => 'us-east-1'
        ]);

        $src = fopen('./dist/piwik.zip', 'r');
        $opts = array(
            "before_upload" => function(Command $command){
                echo "Uploading...\n";
            }
        );

        $uploader = new ObjectUploader($s3, 'crosschx-artifacts', 'piwik/' . $config->version() . '/piwik.zip', $src, 'private', $opts);

        $uploader
            ->promise()
                ->then( function(){
                  echo "Upload finished.";
                })
                ->otherwise( function(){
                  echo "Upload failed!";
                });

        $uploader->upload();

    }
}

class ZipRecursiveFilterIterator extends \RecursiveFilterIterator {

    public static $FILTERS = array(
        './.git',
        './tmp',
        './tests',
        './build',
        './dist',
        './vendor/aws',
        './vendor/phpunit',
        './vendor/facebook',
        './vendor/phpseclib',
        './vendor/symfony/var-dumper',
        './vendor/symfony/yaml',
    );

    public function accept() {
        return !in_array(
            $this->current()->getPath(),
            self::$FILTERS,
            true
        );
    }

}
?>