Piwik + Queue (Browser) Support
-------------------------------

This repo is a fork of the official Piwik project from [Github](https://github.com/piwik/piwik).

This version simply updates the Composer dependency for the [device-detector library](https://github.com/piwik/device-detector) to point to a [custom version](https://bitbucket.org/mpotnick/device-detector) that adds support for our Electron-based application, Queue.

###Installation

* Make sure Composer is installed. The preferred method is through [Homebrew](http://brew.sh/):  
  ```
  brew install composer
  ```
* Make sure you've set up a `~./aws/credentials` file with your access key ID / secret. [More info here](http://docs.aws.amazon.com/aws-sdk-php/v3/guide/guide/credentials.html#using-the-aws-credentials-file-and-credential-profiles).  
* In the root dir, run `composer install` to install dependencies.  
* Run `composer package` to create `./dist/piwik.zip`  
* Run `composer deploy` to upload `piwik.zip` to the `crosschx-artifacts` S3 bucket. The artifact will be placed into `/piwik/{version}/piwik.zip`.  